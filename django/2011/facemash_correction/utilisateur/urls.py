from django.conf.urls.defaults import *


urlpatterns = patterns('utilisateur.views',
    (r'comparer', 'voter'),
	(r'edit/(?P<user>\d+)', 'edit'),
    (r'^$','liste_utilisateurs'),
    (r'^liste_votes$','liste_votes'),
    (r'^nouveau$', 'edit'),
    (r'voir_utilisateur/(?P<user>\d+)', 'voir_utilisateur'),
    (r'voter/(?P<user1>\d+)/(?P<user2>\d+)', 'voter'),
)

