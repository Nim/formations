from django.urls import path
from . import views

app_name = 'utilisateur'
urlpatterns = [
    path('', views.liste_utilisateurs, name='liste_utilisateurs'),
    path('liste_votes/', views.liste_votes, name='liste_votes'),
    path('voir_utilisateur/<int:user>/', views.voir_utilisateur, name='voir_utilisateur'),
    path('comparer/', views.voter, name='voter'),
    path('edit/<int:user>/', views.edit, name='edit'),
    path('nouveau/', views.edit, name='edit_nouveau'),
    path('voter/<int:user1>/<int:user2>', views.voter, name='voter_pour'),
]

