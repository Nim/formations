from utilisateur.models import *
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

from datetime import datetime
from random import randint

def liste_utilisateurs(request):
    utilisateurs = Utilisateur.objects.order_by('-score', 'nom')
    return render(request, 'utilisateur/liste_utilisateurs.html', {'utilisateurs': utilisateurs})

def liste_votes(request):
    votes = Votant.objects.all()
    return render(request, 'utilisateur/liste_votes.html', {'votes': votes})
    
def voir_utilisateur(request, user):
    utilisateur = Utilisateur.objects.get(id=user)
    return render(request, 'utilisateur/voir_utilisateur.html', {'utilisateur': utilisateur})

def edit(request, user=None):
    if user is not None:
        utilisateur = Utilisateur.objects.get(id=user)
    else:
        utilisateur = None
    if request.method == 'POST':
        form = UtilisateurEditForm(request.POST, request.FILES, instance=utilisateur)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
        else:
            int("not valid!")
    else:
        form = UtilisateurEditForm(instance=utilisateur)
    return render(request, 'utilisateur/edit.html', {'form': form})

def voter(request, user1=None, user2=None): #user1 et le/la gagnant du vote
    
    if (user1 is not None) and (user2 is not None):
        utilisateur1 = Utilisateur.objects.get(id=user1)
        utilisateur2 = Utilisateur.objects.get(id=user2)
        utilisateur1.comparaison(1, utilisateur2)
        utilisateur2.comparaison(0, utilisateur1)
        utilisateur1.save()
        utilisateur2.save()
        votant          = Votant()
        votant.ip       = request.META['REMOTE_ADDR']
        votant.voted    = utilisateur1
        votant.notVoted = utilisateur2
        votant.date     = datetime.now()
        votant.save()
        return HttpResponseRedirect("/utilisateur/comparer")
        
    else:
        nbUsers = len(Utilisateur.objects.all())
        if nbUsers <2:
            return HttpResponse("Non mais il faut au moins deux utilisateurs !")
        user1 = randint(1,nbUsers)
        user2 = (randint(1,nbUsers-1) + user1-1)%nbUsers+1 # astuce pour que user2 != user1 sans faire de retirage
        print(user1, user2, nbUsers)
        utilisateur1 = Utilisateur.objects.get(id=user1)
        utilisateur2 = Utilisateur.objects.get(id=user2)
        return render(request, 'utilisateur/vote.html', {'utilisateur1': utilisateur1, 'utilisateur2': utilisateur2})
