from django.contrib import admin

from .models import Utilisateur, Votant


admin.site.register(Utilisateur)
admin.site.register(Votant)
