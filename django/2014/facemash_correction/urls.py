from django.conf.urls.defaults import patterns, include, url
from os.path import join, dirname

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',    
    (r'^admin/', include(admin.site.urls)),
    (r'^',include('utilisateur.urls')),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': join(dirname(__file__), "media") }),

)
