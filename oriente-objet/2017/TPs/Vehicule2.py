class Vehicule:
    def __init__(self):
        self.nb_roues = 4
        self.nb_places = 5
        self.x = 5
        self.y = 5
        self.dir = (1,0)

    def avancer(self, carte):
        newx = self.x + self.dir[0]
        newy = self.y + self.dir[1]

        if (0 <= newx < 15 and 0<= newy < 10):
            self.x, self.y = newx, newy
            return True
        return False

    def tourner(self):
        self.dir = (-self.dir[1], self.dir[0])
        return True

    def __repr__(self):
        return "La voiture est au coordonnées {}:{}".format(self.x, self.y)

    def __str__(self):
        return "o"

"""À COMPLETER
Créer d'autres véhicules ex:
- Voiture se déplace mais comme un véhicule mais ne peux pas aller sur l'eau ou dans les montages
- Bateau ne peux aller que sur l'eau
- la Formule 1, lors qu'elle avance elle ne s'arrête pas avant d'avoir rencontré un obstacle Astuce: super() est pratique ici
"""
