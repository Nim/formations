class Voiture:
    def __init__(self):
        self.nb_roues = 4
        self.nb_places = 5
        self.passager = []

    def monte(self, passager: str):
        """
        Enregistre un passager dans la voiture s'il y a de la place
        :param passager: String, nom du passager
        :return: boolean, True si l'opération réussie, False sinon
        """
        if len(self.passager) < self.nb_places:
            self.passager.append(passager)
            return True
        else:
            return False

    def descend(self):
        """
        Retire un passager de la voiture si sela est possible
        :return: le nom du passagé retiré
        """
        if len(self.passager):
            return self.passager.pop()
        else:
            return None

    def __repr__(self):
        return ("La voiture à {0} roues, {1} places et " +
                ("{2} " if len(self.passager) else "personne n'") +
                ("est" if len(self.passager)<=1 else "sont") +
                " assis à bord").format(
            self.nb_roues, self.nb_places, ", ".join(self.passager))


# Programme de test de votre solution
# Executer simplement le script pour lancer le test.
# Vous ne devriez pas avoir a modifier le code suivant
if __name__ == "__main__":
    v = Voiture()
    if not v.descend():
        print("La voiture est vide.  Resultat OK")
    p = ["     Zil0", "    Zadig", "Kasonnara", "  Bitchos", "    Pibou", "   Lemeda", "   Kaname", "  Someone"]
    r = [True] * 5 + [False] * 3
    for np, nr in zip(p, r):
        print(np, "veut monter a bord. ", end="")
        s = v.monte(np)
        if s:
            print("           Et ça passe!", end="")
        else:
            print("  Mais ça ne passe pas!", end="")
        if s == nr:
            print("  Resultat OK")
        else:
            print("  Resultat FAUX")

    for k in range(6):
        s = v.descend()
        print(s, "descend!  Resultat", "OK" if (s and k<5) or (s is None and k==5) else "FAUX")


