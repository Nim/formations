from corrige import Vehicule3_solved
from corrige.Destructeur3 import Destructeur


class Carte:
    def __init__(self):
        self.carte = """
######....~~~~~
....##....~~~~~
..#.#.....~~~~~
..#.......~~~~~
...........~~~~
....#......~~~~
#...#......~~~~
.......#....~~~
.#...........~~
.#.....#.......
"""
        self.v = Vehicule3_solved.Buldozer()

    def is_inbound(self, x, y):
        return 0 <= x < 15 and 0<= y < 10

    def get_index(self, x, y):
        return x + y * 16 + 1

    def get_at(self, x, y):
        return self.carte[self.get_index(x,y)]

    def display(self):
        voiture_index = self.get_index(self.v.x, self.v.y)
        display_carte = self.carte[:voiture_index] + str(self.v) + self.carte[voiture_index+1:]
        print(display_carte)

c = Carte()

c.display()
while True:
    i = input("(t ou a ou d) >>")
    r = False
    if i == "t":
        r = c.v.tourner()
    elif i == "a":
        r = c.v.avancer(c)
    elif i == "d":
        if isinstance(c.v, Destructeur):
            r = c.v.detruire(c)
    c.display()
    if r:
        print("OK")
    else:
        print("erreur")