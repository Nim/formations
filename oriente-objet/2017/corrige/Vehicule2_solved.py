import random


class Vehicule:
    def __init__(self):
        self.nb_roues = 4
        self.nb_places = 5
        self.x = 5
        self.y = 5
        self.dir = (1,0)

    def avancer(self, carte):
        newx = self.x + self.dir[0]
        newy = self.y + self.dir[1]

        if carte.is_inbound(newx, newy):
            self.x, self.y = newx, newy
            return True
        return False

    def tourner(self):
        self.dir = (-self.dir[1], self.dir[0])
        return True

    def __repr__(self):
        return "La voiture est au coordonnées {}:{}".format(self.x, self.y)

    def __str__(self):
        return "o"


class Voiture(Vehicule):
    def avancer(self, carte):
        newx = self.x + self.dir[0]
        newy = self.y + self.dir[1]

        if carte.is_inbound(newx, newy) and carte.get_at(newx, newy) == ".":
            self.x, self.y = newx, newy
            return True
        return False

    def __str__(self):
        return "V"


class Bateau(Vehicule):
    def __init__(self):
        super().__init__()
        self.nb_roues = 0
        self.nb_places = 500
        self.x = 13

    def avancer(self, carte):
        newx = self.x + self.dir[0]
        newy = self.y + self.dir[1]

        if carte.is_inbound(newx, newy) and carte.get_at(newx, newy) == "~":
            self.x, self.y = newx, newy
            return True
        return False

    def __str__(self):
        return "B"


class Formule1(Voiture):
    def avancer(self, carte):
        r = False
        while super().avancer(carte):
            r = True
        return r

    def __str__(self):
        return "F"


class TARDIS(Vehicule):
    def avancer(self, carte):
        self.x, self.y = random.randint(0, 14), random.randint(0, 14)
        return True

    def __init__(self):
        super().__init__()
        self.nb_roues = 0
        self.nb_places = 6
