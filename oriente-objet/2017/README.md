#Formation Programmation Orienté Objet 2017

##Diapos
Les sources Latex sont disponnibles dans `sources_Latex_diapos`<br>
compiler avec la commande `make` <br>
Requière certains paquets comme `texlive-xetex`, `texlive-luatex` et `texlive-lang-european`
##TPs :
#####TP1 : <br>
Compléter le code Voiture1.py en écrivant les fonctions monte et descend.

#####TP2 :<br>
Executez map2.py <br>
Compléter le code Vehicule2.py avec de nouveau véhicule. <br>
Créer d'autres véhicules exemple: <br>

  - Voiture se déplace comme un véhicule mais ne peux pas aller sur l'eau ou dans les montages.
  - Bateau ne peux aller que sur l'eau.
  - Une Formule 1, comme voiture mais lorsqu'elle avance, elle ne s'arrête pas avant d'avoir rencontré un obstacle Astuce: super() est pratique ici.

Astuce : interessez vous a la classe carte de map2.py, en particulier les opération qu'elle met a votre disposition  

#####TP3 : <br>
S'intéresser à map3.py et Destructeur3.py <br>
Compléter le code Vehicule2.py Pour un ajouter un Buldozer qui peut détruire le terrain. <br>
