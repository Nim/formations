#include <stdio.h>

int subfunction(int x, int y) {
    return x + y;
}

int main() {
    int x = 2;
    int y = 3;
    int z = subfunction(x, y);
    return x * y + z;
}
