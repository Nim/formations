---
title: Cryptographie
subtitle: Principes et applications
shorttitle: CryptoParty
author: Guilhem Saurel
date: 2016-04-13
LAAS: fr
mainfont: Source Serif Pro
sansfont: Source Sans Pro
monofont: Source Code Pro
revealjs-url: https://cdn.jsdelivr.net/reveal.js/3.0.0/
link-citations: true
toc-depth: 0
lang: fr
---

# Théorie

## Vocabulaire

- Cryptologie: Science du secret
- Cryptographie: Science du secret des écrits
- Cryptogramme: Message codé
- Chiffrer: Message clair + algorithme et/ou clef secrète → cryptogramme
- Déchiffrer: Cryptogramme + algorithme et/ou clef secrète → message clair
- Décrypter: cryptogramme → message (attaque, généralement difficile)
- Cryptanalyse: Science du décryptage
- Cryptoparty: Manger des pizzas

## Non-Vocabulaire

- Crypter: mettre dans une crypte
- Cryptage: ?

## Cryptographie symétrique

- rot13

```python
message = "coucou secret"
cryptograme = message.encode("rot13")
# "pbhpbh frperg"
message == cryptogramme.encode("rot13")
```

- One time pads

```python
message = "coucou secret"
clef = "bépo > azerty"
def xor_strings(xs, ys):
    return "".join(chr(ord(x) ^ ord(y))
                   for x, y in zip(xs, ys))
cryptogramme = xor_strings(message, clef)
# 0x01 8605 0c4f 4b00 121f 0600 110d
message == xor_strings(cryptogramme, clef)
```

## Cryptographie asymétrique

@diffiehellman

@RSA

## Clef privée, clef publique

Alice génère une paire de clef, et donne sa clef publique à Bob.

- Elle peut signer un document, et Bob vérifier la signature
- Bob peut chiffrer un document, que seulle Alice pourra déchiffrer

## La signature

- Document à signer → empreinte
- Empreinte + clef privée → signature
- Empreinte + signature + clef publique → vérification

## Le chiffrement

- Document en clair + clef publique → cryptogramme
- Cryptogramme + clef privée → document en clair

## Algorithmes & longueurs

- DSA
- [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Example) (1024, 2048, 3072, 4096, 8192)
- ECDSA (128, 256, 512)
- Ed25519

## Utilisations

- HTTPS
- SSH
- PGP
- Git

# Pratique

## Nombre aléatoires

- Entropie

## HTTPS

```bash
openssl genrsa -out $SITE.key 2048
openssl req -new -key $SITE.key -out $SITE.csr
cat $SITE.csr
vim $SITE.startssl.crt
```

## SSH

```bash
ssh-keygen -t ed25519
```

\tiny{\texttt{Generating public/private ed25519 key pair. \\
Enter file in which to save the key (\textasciitilde/.ssh/id\_ed25519): \\
Enter passphrase (empty for no passphrase): \\
Enter same passphrase again: \\
Your identification has been saved in \textasciitilde/.ssh/id\_ed25519. \\
Your public key has been saved in \textasciitilde/.ssh/id\_ed25519.pub. \\
The key fingerprint is: \\
SHA256:JEr7Q6sujNlbUMBPpskIRdnsAiM6V+of9It5duZsINw nim@Nausicaa
}}

## SSH: Résultat

\tiny{\texttt{ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKsuunzXeJXTGozCSGTM7jBWa6/Axo2j0yf7VnoB2m+U nim@Nausicaa \\
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3AXpKOFbFyNIriOY99a6iLW/Hij9pupIx/1nxm4/gODleh+brYO15izyYdnGoL0ZBehN61wWW89Ldw9CpZsghfNFjjPb5C3YowY/ZmHbuSxom9oDCSmGswjgT9QHUifXUSwFkLtbgH26yylN1g0U/8bUy3cn0onyKrv8z5inEUjXKWRSVYxFAshz6QtE0zUHW9wZnOPYE2RVnsRIElotkzoYeQc8b5IO8J9XnVP408S7uxiaw8uFMdB+xh3TtREbGrkVT4s5oKCor8E7z22mZVmmymYJvJ35sTzNsJbUyhUeqKP4uuf7LsNZZty+p8wibXzk6GQzBYtmNM5XWU12H nim@Nausicaa
}}

## PGP: Générer une clef

```bash
gpg --gen-key
KEY=2D1B4FEA
gpg --fingerprint $KEY
```

\tiny{\texttt{pub   4096R/2D1B4FEA 2016-04-13 [expire : 2016-05-13] \\
      Empreinte de la clef = 4254 CD6F ABFC E24D DA4A  F263 7DCD 81B7 2D1B 4FEA \\
      uid                  Guilhem Saurel (Example Key - DO NOT USE) <guilhem@saurel.me> \\
      sub   4096R/EA31CDAD 2016-04-13 [expire : 2016-05-13]
}}

## Ajouter d’autres UIDs

```bash
gpg --edit-key $KEY
> adduid
> uid <uid>
> trust
> save
```


## Exporter la clef

```bash
KEY=2D1B4FEA
gpg --armor --export $KEY > $KEY.asc
scp $KEY.asc perceval:/clubs/n7/net7/www_public/pgp/
```

## Signer d’autres clefs

```bash
gpg --import *.asc
OTHER_KEY=4653CF28
gpg --fingerprint $OTHER_KEY
```

**CHECK THE FUCKING FINGERPRINT**

```bash
gpg --sign-key $OTHER_KEY
```

Alternative:

```bash
gpg --edit-key $OTHER_KEY
> fpr
> sign
> check
> save
```

## Signer un document

```bash
DOC=pipo.txt
vim $DOC
gpg --armor [--output $DOC.sig] --detach-sign $DOC
```

NB: Ça marche pour *tout* type de document

## Vérifier une signature

```bash
gpg --verify $DOC.sig [$DOC]
```

## Chiffrer

```bash
gpg --armor -r $OTHER_KEY --encrypt $DOC
```

NB: Ça marche pour *tout* type de document

## Déchiffrer

```bash
gpg --decrypt $DOC.asc
```

## Serveurs de clefs

```bash
gpg --recv-keys $KEYS
gpg --send-key $KEYS
gpg --refresh-keys [$KEYS]
```

## Révocation

```bash
gpg --output revoke-$KEY.asc --gen-revoke $KEY
```

## Stats & Graphes

Debian: paquet signing-party

```bash
gpg --list-sigs $KEYS | sig2dot | springgraph > gpg.png
```

Sinon, [en version web](https://saurel.me/PGP/net7/graph)

# Pratique

## Emails

- enigmail
- gare aux en-têtes
- destinataires multiples
- mailing-list
- Django

## Logiciel Libre

- Révision par des pairs
- **Don't roll your own**
- Vie privée

## Aller plus loin

Choses pas abordées:

- préférences sur les algorithmes
- caff
- Yubikeys

## Slides

- [Maxima & palkeo](https://www.bde.enseeiht.fr/clubs/net7/supportFormations/cryptographie/2013/formation.pdf)
- [Votre serviteur](https://www.bde.enseeiht.fr/clubs/net7/supportFormations/cryptographie/2016/pgp.pdf)

## Références
