def degree2radian(angle, afficher=False):
    """Convertit en radian un angle en degré"""
    result = angle * 3.141592 / 180
    if afficher:
        print(result)
    return result
