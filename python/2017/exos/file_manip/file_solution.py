#!/usr/bin/env python3


def build_outname(filename):
    """Pour conserver l'extenson du fichier en sortie"""
    out = filename.split('.')
    if len(out) == 1:
        out += "_reversed.txt"
    else:
        out = out[0] + "_reversed." + out[1]
    return out


def reverse_file(filename):
    with open(filename, 'r') as f:
        lines = f.read()[::-1]
        lines = lines[1:]
    with open(build_outname(filename), 'w') as f:
        for i, l in enumerate(lines.splitlines()):
            f.write('{} {}\n'.format(i, l))


reverse_file("file.txt")
