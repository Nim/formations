!#/usr/bin/env python3

# Grâce à la ligne ci-dessus, ce script peut être exécuté directement dans un terminal en tapant `./strings_solution.py`
# Ne pas oublier de le rendre exécutable au préalable avec `chmod +x strings_solution.py`

s = "iPq le cYanure dans le THé c'est pas bON..."

print("Question 1 : ", end='')
result = s.count(' ') + 1
result2 = len(s.split())
assert result == result2
print(result)

result = ''
for c in s:
    if c.isupper():
        result += c
result = result.lower()
result_equivalent = ''.join([c for c in s if c.isupper()]).lower()
assert result == result_equivalent
print("Question 2 : {}".format(result))

result = s.replace("le", '', 1)
result = result[:-3] + '!' # bof, le mieux est de réutiliser replace
result = result.split()[1:]
result = [w for w in result if 'a' not in w]
print("Question 3 :", ' '.join(result))

print("Question 4 :")
d = {}
for c in s:
    if c.isalpha():
        if not d.get(c):
            d[c] = 1
        else:
            d[c] += 1
        # d[c] = 1 if not d.get(c) else d[c] + 1

for k in d:
    print("La lettre {} apparaît {} fois.".format(k, d[k]))
