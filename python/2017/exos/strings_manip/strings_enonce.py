s = "iPq le cYanure dans le THé c'est pas bON..."

"""
Énoncé :

    1) Compter le nombre de mots.
    2) Afficher "python" sans écrire une lettre de ce mot.
    3) Enlever le premier mots, ainsi que les mots comprenant un 'a', et le premier 'le'. Remplacer les '...' par un '!'.
    4) Compter le nombre d'occurences de chaque lettres. Ne pas inclure les espaces et la ponctuation. Sortie attendue :
        La lettre q apparaît 1 fois.
        La lettre l apparaît 2 fois.
        La lettre e apparaît 4 fois.
        ...
"""
