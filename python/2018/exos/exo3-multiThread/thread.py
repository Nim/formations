#!/usr/bin/env python3
from threading import Thread, RLock
# on importe la classe Thread et RLock du module thread
import sys, time, random
# on importe d'autre module utile

lock = RLock()  ### on la définie en variable globale

class Displayer(Thread):  # la classe doit hériter de Thread
    def __init__(self, letter):
        Thread.__init__(self)  # appel du constructeur père
        self.letter = letter

    def run(self):
        for i in range(5):
            with lock:  ### on verrouille
                sys.stdout.write(self.letter)  # on ecrit sur la console
                sys.stdout.flush()  # on affiche le texte
            wait = random.random()/100
            time.sleep(wait)


display1 = Displayer("abcd")
display2 = Displayer("1234")

display1.start()
display2.start()

display1.join()
display2.join()
