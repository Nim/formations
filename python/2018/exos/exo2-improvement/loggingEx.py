#!/usr/bin/env python3
import logging


def run():
    logging.basicConfig(
        filename='monApp.log',  # le fichier de log
        level=logging.DEBUG,  # niveau de log (DEBUG/INFO/...)
        format='%(asctime)s - %(name)s - ' 
    	+ '%(levelname)s - %(message)s',
    )
    # différents niveau de message
    # debug, info, warning, error, critical
    logging.debug("Mon debug")

if __name__ == '__main__':
    run()
