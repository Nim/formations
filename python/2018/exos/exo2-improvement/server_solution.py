#!/usr/bin/env python3

import socket
import argparse
import logging
import hashlib
import base64
from getpass import getpass


class Server:  # Création de la classe Server
    def __init__(self, args):  # Constructeur
        self.host = args.host
        self.port = args.port
        ##  logging
        level = logging.INFO  # met le niveau de log à INFO par default
        if args.debug:  # si en mode debug
            level = logging.DEBUG  # mise au niveau debug des logs
        logging.basicConfig(
            filename=args.output,
            level=level,
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        )
        ##  socket
        self.main_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # instancie un object de la classe socket
        self.main_conn.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # corrige un bug
        self.main_conn.bind((self.host, self.port))
        # prépare le socket a écouter sur le port et l'host choisis
        self.main_conn.listen(5)
        # met jusqu'à 5 clients en attente de connection
        logging.info(
            "Le serveur écoute à présent sur le port {}".format(self.port))
        ## passwd
        self.secret = args.secret
        self.passwd = ''
        if self.secret:
            self.passwd = hashlib.sha512(getpass('Entrer le mdp du chat : ')\
                .encode()).hexdigest()

    def __del__(self):  # Destructeur
        logging.info("Fermeture de la connection")
        try:
            self.client_conn.close()  # fermeture de la connection client
        except AttributeError:
            pass
        self.main_conn.close()  # fermeture de la connection serveur

    def start(self):
        self.client_conn, self.infos_conn = self.main_conn.accept()
        ip, port = self.infos_conn
        logging.info(
            "Tentative de connection de {} par {}".format(ip, port))
        msg_recv = b""
        while msg_recv != b"fin":
            msg_recv = self.client_conn.recv(1024)
            # mise en attente de reception d'un msg (longeur max 1024)
            logging.debug(
                "Message recu de {}:{} {}".format(
                    self.infos_conn[0],
                    self.infos_conn[1],
                    msg_recv.decode(),
                )
            )
            self.client_conn.send(b"5 / 5")


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--port', '-p', type=int, nargs='?', default=12800,
        help='port de connection'
    )
    parser.add_argument(
        '--host', '-x', type=str, nargs='?', default='localhost',
        help='hôte de connection'
    )
    parser.add_argument(
        '--debug', '-d', action='store_true', default=False,
        help='affiche les logs de debug'
    )
    parser.add_argument(
        '--output', '-o', type=str, default=None,
        help='enregistre les log dans le fichier OUTPUT'
    )
    parser.add_argument(
        '--secret', '-s', action='store_true', default=False,
        help='protège avec un mdp la communication'
    )
    args = parser.parse_args()
    server = Server(args)
    server.start()


if __name__ == '__main__':
    run()
