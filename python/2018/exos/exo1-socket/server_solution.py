import socket


class Server:  # Creation de la classe Server
    def __init__(self):  # Constructeur
        self.hote = ''
        self.port = 12800
        self.main_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.main_conn.bind((self.hote, self.port))
        self.main_conn.listen(5)  # met jusqu'a 5 client en attente de connection
        print("Le serveur écoute à présent sur le port {}".format(self.port))

    def __del__(self):  # Destructeur
        print("Fermeture de la connection")
        try:
            self.client_conn.close()
        except AttributeError:
            pass
        self.main_conn.close()

    def start(self):
        self.client_conn, self.infos_conn = self.main_conn.accept()
        ip, port = self.infos_conn
        print("Nouvelle connection sur le port {} avec l'ip {}".format(port, ip))
        msg_recu = b""
        while msg_recu != b"fin":
            msg_recu = self.client_conn.recv(1024)  # attend un msg de longeur max 1024
            print(msg_recu.decode())
            self.client_conn.send(b"5 / 5")


server = Server()  # initialisation de la classe Server (appel du constructeur)
server.start()
del server
