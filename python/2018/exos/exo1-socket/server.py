import socket


class Server:  # Creation de la classe Server
    def __init__(self):  # Constructeur
        ...
        ...
        self.main_conn.listen(5)  # met jusqu à 5 client en attente de connection
        print("Le serveur écoute à présent sur le port {}".format(self.port))

    def __del__(self):  # Destructeur
        print("Fermeture de la connection")
        ...

    def start(self):
        ...
        ???

server = Server()  # initialisation de la classe Server (appel du constructeur)
server.start()

##############################################################################
# Block 1 ####################################################################
#----------------------------------------------------------------------------#
self.client_conn, self.infos_conn = self.main_conn.accept()
ip, port = self.infos_conn
print("Nouvelle connection sur le port {} de l ip {}".format(port, ip))

##############################################################################
# Block 2 ####################################################################
#----------------------------------------------------------------------------#
self.hote = ''
self.port = 12800

##############################################################################
# Block 3 ####################################################################
#----------------------------------------------------------------------------#
self.main_conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
self.main_conn.bind(($, $))

##############################################################################
# Block 4 ####################################################################
#----------------------------------------------------------------------------#
try:
    self.client_conn.close()
except AttributeError:
    pass
self.main_conn.close()
