import socket

class Server:  # Creation de la classe Server
    def __init__(self):  # Constructeur
        self.hote = ''
        self.port = 12800
        self.main_conn = socket.socket(
					socket.AF_INET, 
					socket.SOCK_STREAM)
        self.main_conn.bind((self.hote, self.port))
        self.main_conn.listen(5)
        # met jusqu'a 5 client en attente de connection
        print("Le serveur écoute à présent sur le port {}"
				.format(self.port))

    def __del__(self):  # Destructeur
        print("Fermeture de la connection")
        try:
            self.client_conn.close()
        except AttributeError:
            pass
        self.main_conn.close()
