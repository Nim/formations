>>> ./server.py -h
usage: server.py [-h] [--port [PORT]] [--host [HOST]] [--debug]
                 [--output OUTPUT] [--secret]

optional arguments:
  -h, --help            show this help message and exit
  --port [PORT], -p [PORT]
                        port de connection
  --host [HOST], -x [HOST]
                        hôte de connection
  --debug, -d           affiche les logs de debug
  --output OUTPUT, -o OUTPUT
                        enregistre les logs dans le fichier OUTPUT
  --secret, -s          protège avec un mdp la communication
