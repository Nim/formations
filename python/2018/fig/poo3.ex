class Ecocup(Verre):
	def __init__(self, volume, graduation):  # méthode constructeur
		super().__init__(volume)  # appel du constructeur mère
		# équivalent à Verre.__init__(self, volume)
		self.graduation = graduation

	def qbool(self):
		"""méthode qbool, qui fait prendre qbool"""    
		self.contenue = 0
