import argparse  # on importe le module argparse
def run():
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'--port', '-p', type=int, nargs='?', default=12800,
		help='port de connection',
	)
	parser.add_argument(
		'--debug', '-d', action='store_true', default=False,
		help='Affiche les logs de debug',
	)
	args = parser.parse_args()
	# args.debug == True si -d a été ajouté au lancement
