from threading import Thread
# on importe la classe Thread du module thread
import sys, time, random
# on importe d'autre module utile

class Displayer(Thread):  # la classe doit hériter de Thread
  def __init__(self, letter):
    Thread.__init__(self)  # appel du constructeur père
    self.letter = letter

  def run():
    for i in range(5):
      sys.stdout.write(self.letter)  # on ecrit sur la console
      sys.stdout.flush()  # on affiche le texte
      wait = 0.2 + random.random()/10
      time.sleep(wait)
