class Verre:
	nombre = 0  # compteur de verre
	def __init__(self, volume):  # méthode constructeur
		Verre.nombre += 1
		self.volume = volume
		self.contenue = 0
	
	def __del__(self):  # méthode destructeur
		Verre.nombre -= 1
	
	def remplir(self, qqt):
		"""méthode remplir, qui rempli le verre"""
		self.contenue = max(qqt, self.volume)

	def boire(self, qqt):  # méthode boire
		self.contenue = min(0, self.contenue - qqt)
