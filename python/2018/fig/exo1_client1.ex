import socket


class Client:  # Creation de la classe Client
    def __init__(self):  # Constructeur
        self.hote = 'localhost'
        self.port = 12800
        self.main_conn = socket.socket(
					socket.AF_INET, 
					socket.SOCK_STREAM)
        self.main_conn.connect((self.hote, self.port))  
        # se connecte au serveur
        print("Connexion établie avec le serveur sur le port {}"
					.format(self.port))

    def __del__(self):  # Destructeur
        print("Fermeture de la connection")
        self.main_conn.close()
