#include <iostream>
#include <array>
#include <vector>
#include <algorithm>
#include <iterator>

const std::size_t N{7};

int main() {

    std::array<int, N> A;
    { // Initialize A
        int k = 0, s = 0;
        std::generate(A.begin(), A.end(), [&k, &s]() { return s += ++k; });
    }

    std::vector<int> B;
    B.resize(N);
    std::transform(A.begin(), A.end(), B.begin(), [](auto e) { return e * e; });

    std::copy(A.rbegin(), A.rend(), std::back_inserter(B));

    std::cout << std::hex << std::showbase;
    std::copy(B.begin(), B.end(), std::ostream_iterator<int>(std::cout, "\t"));

    return 0;
}
