\documentclass{beamer}

\usepackage{polyglossia}
\setdefaultlanguage{english}
\usepackage{fontspec}

% {{{ Beamer
\setbeamertemplate{footline}[frame number]
\beamertemplatenavigationsymbolsempty

\mode<presentation> {
  \usetheme{Madrid}
}
% }}}


% {{{ Minted
\usepackage[]{minted}
\usepackage[skins,minted]{tcolorbox}

\definecolor{cppbg}{rgb}{0.05,0.05,0.05}
\usemintedstyle{monokai}

\newmintinline[icpp]{C++}{bgcolor=cppbg}
\newtcblisting{cpp}[2][]{
  listing engine=minted,
  listing only,
  #1,
  title=#2,
  minted language=C++,
  minted options={fontsize=\footnotesize},
  colback=cppbg
}
\newtcblisting{cppp}[2][]{
  listing engine=minted,
  listing only,
  #1,
  title=#2,
  minted language=C++,
  minted options={fontsize=\tiny},
  colback=cppbg
}

% }}}

\newcommand{\cppvsjava}[3]{
  \begin{center}
    {\Huge #1}
  \end{center}
  \vfill
  \begin{block}{Cpp}
    #2
  \end{block}
  \begin{block}{Java}
    #3
  \end{block}
}

\newenvironment{Notes}
{\begin{block}{Notes}\begin{itemize}}
{\end{itemize}\end{block}}

\title{Formation Cpp}
\author{B. \textsc{Lemarchand}}
\date{May 11, 2016}
\subject{Présentation}


\AtBeginSection[]{%
  \begin{frame}
    \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
      \usebeamerfont{title}\insertsectionhead\par%
    \end{beamercolorbox}
  \end{frame}
}


\begin{document}


\begin{frame}
  \titlepage

  \begin{center}
    \footnotesize{Presentation available on\\
    \url{http://bde.enseeiht.fr/~lemarcb/res/cpp.pdf}}
  \end{center}
\end{frame}


\section{Introduction}


\begin{frame}{C/C++}
  \begin{figure}
      \includegraphics[width=0.2\textwidth]{fig/gnu.jpg}
      \hfill
      {\Huge C $\neq$ C++}
      \hfill
      \includegraphics[width=0.2\textwidth]{fig/cow.jpg}
  \end{figure}

  \begin{block}{The truth is...}
    \begin{itemize}
      \item C and C++ may look the same but have nothing in common.
      \item C++ isn't wrapped C.
      \item C/C++ does not exist !
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}[fragile]{Are you convinced now ?}
  \begin{figure}
    \includegraphics[height=0.8\textheight]{fig/cpp_ex.png}
  \end{figure}
\end{frame}


\begin{frame}{An evolving language}
  \begin{figure}[h]
    \centering
      \includegraphics[height=0.7\textheight]{fig/wg21-timeline.png}
    \caption{\large{\url{https://isocpp.org/std/status}}}
  \end{figure}
\end{frame}


\begin{frame}{Cpp vs Java}
  \only<1>{
    \cppvsjava{Focus}
    {Execution efficiency.}
    {Developer productivity.}
  }
  \only<2>{
    \cppvsjava{Freedom}
    {Trusts the programmer.}
    {Imposes some constraints to the programmer.}
  }
  \only<3>{
    \cppvsjava{Memory Management}
    {\begin{itemize}
      \item Arbitrary memory access possible.
      \item Memory freed with RAII.
    \end{itemize}}
    {\begin{itemize}
      \item Memory access only through handles.
      \item Garbage collection.
    \end{itemize}}
  }
  \only<4>{
    \cppvsjava{Code}
    {Concise expression.}
    {Explicit operation.}
  }
  \only<5>{
    \cppvsjava{Type System}
    {Everything is resolved at compile time.}
    {Type resolution is mixed between compile and execution type.}
  }
  \only<6>{
    \cppvsjava{Type Safety}
    {Type casting is explicit.}
    {Implicit cast can appear between compatible types.}
  }
  \only<7>{
    \cppvsjava{Programming Paradigm}
    {Object-oriented and multi paradigm.}
    {Object-oriented.}
  }
  \only<8>{
    \cppvsjava{Operators}
    {Operator overloading.}
    {Meaning of operators immutable.}
  }
  \only<9>{
    \cppvsjava{Genericity}
    {Turing complete template system.}
    {Approximative.}
  }
  \only<10>{
    \cppvsjava{Main Advantage}
    {Powerful capabilities of language.}
    {Feature-rich, easy to use and widespread libraries.}
  }
\end{frame}


\begin{frame}{Tools}
  \begin{columns}[onlytextwidth, t]
    \begin{column}{0.6\textwidth}
      \begin{block}{Editor}
        \begin{itemize}
          \item Vim + syntastic + YouCompleteMe
          \item Code::Blocks, Visual Studio, \dots
        \end{itemize}
      \end{block}

      \begin{block}{Compiler}
        \begin{itemize}
          \item g++
          \item clang++
        \end{itemize}
      \end{block}

      \begin{block}{And\dots interpreter}
        \begin{itemize}
          \item cling
        \end{itemize}
      \end{block}
    \end{column}

    \begin{column}{0.35\textwidth}
      \begin{figure}
        \includegraphics[width=\linewidth]{fig/tools.jpg}
      \end{figure}
    \end{column}
  \end{columns}
\end{frame}

\section{Syntax and First Programs}


\begin{frame}[fragile]{Hello world!}
  \begin{cpp}{main.cpp}
#include <iostream>

int main() {
    std::cout << "Hello world!" << std::endl;
    return 0;
}
  \end{cpp}

  \begin{Notes}
    \item same \icpp+#include+ in C
    \item same \icpp+main+ in C
    \item Use streams instead of \icpp+printf+
  \end{Notes}
\end{frame}


\begin{frame}[fragile]{Input/Output (term)}
  \begin{cpp}{main.cpp}
// ...
sdt::string name, desc; int age;
std::cin >> name >> age;
getline(std::cin, desc);

std::cout << "Hi guys!\n" << "I'm " << name
    << " and I'm " << age << " years-old.\n"
    << desc << std::endl;
// ...
  \end{cpp}

  \begin{Notes}
    \item You can declare a stream on a file
    \item Streams can be configured
    \item Streams can be iterated!
  \end{Notes}
\end{frame}


\begin{frame}[fragile]{Strings}
  \begin{cpp}{greetings.cpp}
#include <string>

void test_string() {
    std::string greetings = "Hello world";
    greetings[6] = 'W';
    greetings += "!";
    if (greetings == "Hello World!") {
        std::cout << greetings;
    }
}
  \end{cpp}

  \begin{Notes}
    \item The \icpp+char*+ are obsolete in favour of \icpp+std::string+
  \end{Notes}
\end{frame}


\begin{frame}[fragile]{Assignation}
  \begin{cpp}{Syntax}
int age = 22;
int& a_ref_on_age = age;

int size1 = myarray.size(); // ok
int size2 {myarray.size()}; // does not work
std::size_t size3 {myarray.size()}; // better
auto size4 = myarray.size(); // even better
  \end{cpp}

  \begin{Notes}
    \item AAA: Almost Always use Auto
    \item ECB: Else use Curly Braces
  \end{Notes}
  {\footnotesize \url{https://herbsutter.com/2013/08/12/gotw-94-solution-aaa-style-almost-always-auto/}}
\end{frame}


\newsavebox{\raiibox}
\begin{lrbox}{\raiibox}
    \begin{cpp}{Syntax}
try {
    std::string name;
    std::cin >> name;
    throw std::exception("Sthg bad happened");
} catch(std::exception& e) {
    std::cerr << e.what() << "\n" << "'name' have been freed";
}
    \end{cpp}
\end{lrbox}


\begin{frame}[fragile]{RAII}
  \only<1>{
    \usebox{\raiibox}
    \begin{figure}
      \includegraphics[height=0.25\textheight]{fig/trash.jpg}
    \end{figure}
  }

  \only<2>{
    \emph{Fact:} The destructor is systematically called at the end of life.
    \begin{block}{Resource Acquisition Is Initialisation}
      \begin{itemize}
        \item Principle: a resource (dynamic memory, file, lock, etc.) is acquired in
          a constructor and freed in a destructor.
        \item \icpp+std::string, std::vector, std::fstream+
        \item \icpp+std::unique_ptr, std::shared_ptr+
        \item \icpp+std::unique_lock, std::lock_guard+
        \item Benefit: destructors are deterministic ! No more garbage
          collection.
        \item No need of \mintinline[bgcolor=cppbg]{java}+finally+ (Java, C\#,
          JS) or \mintinline[bgcolor=cppbg]{python}+with+ (Python).
      \end{itemize}
    \end{block}
  }
\end{frame}


\begin{frame}[fragile]{Functions}
  \begin{cpp}{Syntax}
return_type myfunction(type1 arg1,/* ...,*/ typeN argN);

auto myautofunction(int a, int b); // cpp14 (use with caution)

auto myotherautofunction(auto a, auto b); // cpp17 (even more)
  \end{cpp}

  \begin{Notes}
    \item Functions can be overloaded.
    \item Functions must be prototyped.
    \item Warning: copies vs references.
    \item Piece of advice: make short functions (no more than a page); \\
      SRP (single responsibility principle).
  \end{Notes}
\end{frame}


\begin{frame}[fragile]{Data structures}
  \begin{cpp}{Syntax}
std::array<std::string, 4> = {"A", "AB", "ABC", "ABCD"};
std::vector<int> v = {0, 1, 1, 2, 3, 5, 8};
std::unordered_map<std::string, std::string> lvl =
    {{"Maxima", "over9000"}, {"toffan", "newbie"}};
std::stack<int> s = {25, 16, 9};
  \end{cpp}

  \begin{Notes}
    \item \icpp+std::array+ is a static array.
    \item \icpp+std::vector+ is a dynamic array.
    \item \icpp+std::unordered_map+ is an associative table.
    \item \icpp+std::stack+ is a generic stack.
  \end{Notes}
\end{frame}


\begin{frame}[fragile]{Loops}
  \begin{cpp}{Syntax}
std::vector<int> v {0, 1, 1, 2, 3, 5, 8, 13, 21};

for (std::size_t i {0}; i != v.size(); ++i)
    // Do sthg with i or v[i]

for (std::vector<int>::iterator it {v.begin()}; it != v.end(); ++it)
for (auto it = v.begin(); it != v.end(); ++it)
    // Do sthg with it or *it

for (int e: v)
for (auto e: v)
    // Do sthg with e
  \end{cpp}

\end{frame}


\begin{frame}[fragile]{Let's play !}
  \begin{block}{Exercices}
    \begin{itemize}
      \item Put N terms of \{1, 3, 6, 10, 15, ...\} in \icpp+std::array+ A.
      \item Put the square of the element of A in \icpp+std::vector+ B.
      \item Copy the elements of A at the end of B in reverse order.
      \item Print the elements of B in hexa separated by tabulations.
    \end{itemize}
  \end{block}

  \pause
  \vfill

  \begin{center}
    {\Huge Stop fooling around, \\
    use the \verb$<algorithm>$ library !}
  \end{center}
\end{frame}

\section{Even more features}


\begin{frame}[fragile]{OOP header}
  \begin{cpp}{Syntax car.hpp}
class Car {
public:
    Car(unsigned max_speed, unsigned speed = 0);
    ~Car();
    void accelerate(unsigned d);

private:
    const unsigned _max_speed;
    unsigned _speed;
}; // ← don't forget it, you fools !
  \end{cpp}

  \begin{Notes}
    \item You can use \icpp+struct+.
    \item You can use inheritance.
  \end{Notes}
\end{frame}


\begin{frame}[fragile]{OOP code}
  \begin{cpp}{Syntax car.cpp}
#include "car.hpp"
#include <iostream>

Car::Car(unsigned max_speed, unsigned speed):
_max_speed {max_speed},
_speed {speed} {
    std::cout << "Car built !" << std::endl;
}

Car::~Car() {/* nothing to do */}

void Car::accelerate(unsigned d) {
    _speed = _speed + d > _max_speed ? _max_speed : _speed + d;
}
  \end{cpp}
\end{frame}


\newsavebox{\constcorrectnessbox}
\begin{lrbox}{\constcorrectnessbox}
    \begin{cpp}{Example}
bool verify(const std::map<std::string, int>& accounts);

std::map<std::string, int> accounts;
// ...

verify(accounts);
    \end{cpp}
\end{lrbox}


\begin{frame}[fragile]{const-correctness}
  \begin{Notes}
    \item A \verb$const$ object is constant during its utilisation.
    \item $\approx$ A \verb$const$ object is only-readable.
    \item const-correctness makes your code safe !
  \end{Notes}

  \only<1>{
    \begin{figure}
      \includegraphics[height=0.5\textheight]{fig/const.jpg}
    \end{figure}
  }

  \only<2>{\usebox{\constcorrectnessbox}}
\end{frame}


\begin{frame}[fragile]{Templates}
  \begin{cpp}{Syntax}
template<typename T>
int cmp(const T& a, const T& b) {
    return a > b ? 1 : a < b ? -1 : 0;
}
// ...
cmp<std::string>("Salut", "Bonjour");
  \end{cpp}

  \begin{Notes}
    \item Errors can be a bit complicated
    \item You can template types or values
    \item All templates are resolved at compilation
    \item Play with the STL =)
  \end{Notes}
\end{frame}


\begin{frame}[fragile]{Miscellaneous}
  \begin{block}{Smart pointers}
    \begin{itemize}
      \item Object oriented pointers.
      \item RAII make them safe.
      \item $\Rightarrow$ Almost \textbf{never} use \icpp+new+ and \icpp+delete+.
    \end{itemize}
  \end{block}

  \begin{block}{Exceptions}
    \begin{itemize}
      \item \mintinline[bgcolor=cppbg, fontsize=\footnotesize]{C++}+try { /* ... */ } catch (std::exception& e) { /* ... */ }+
      \item Exceptions inherit from \icpp+std::exception+
      \item Destructors are systematically called
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}[fragile]{obsolete C++ vs. modern C++}
  \begin{columns}[onlytextwidth, t]
    \begin{column}{0.49\textwidth}
      \begin{cppp}{obsolete C++}
HRESULT CreateNotifyIcon(NotifyIcon** ppres) {
    NotifyIcon*      icon = 0;
    HRESULT          hr = S_OK;

    if (SUCCEEDED(hr)) {
        icon = new (nothrow) NotifyIcon();
        if (!icon) hr = E_OUTOFMEM;
    }

    if (SUCCEEDED(hr))
        hr = icon->set_text("Blah blah blah");

    if (SUCCEEDED(hr))
        hr = icon->set_visible(true);

    if (SUCCEEDED(hr)) {
        *ppResult = icon;
        icon = NULL;
    } else {
        *ppres = NULL;
    }

    if (icon) delete icon;
    return hr;
}
      \end{cppp}
    \end{column}

    \begin{column}{0.49\textwidth}
      \begin{cppp}{modern C++}
shared_ptr<NotifyIcon> CreateNotifyIcon() {
    auto icon = std::make_shared<NotifyIcon>();

    icon->set_text("Blah blah blah");
    icon->set_visible(true);

    return icon;
}
      \end{cppp}

      \begin{block}{Equivalent codes but}
        \begin{itemize}
          \item Use exceptions for error handling.
          \item Use of RAII to manage these errors.
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{And also...}
  \begin{block}{Always more}
    \begin{itemize}
      \item Namespaces
      \item Multi-inheritance
      \item Constexpr
      \item Variadic templates
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}{Conclusion}
  \begin{block}{When to use cpp?}
    \begin{itemize}
      \item If you need performance
      \item If you need a precise management of memory
      \item If you wanna have fun
    \end{itemize}
  \end{block}

  \begin{block}{When not to use cpp?}
    \begin{itemize}
      \item If you are in a rush
      \item For scripting
      \item If you're not sure of what you want
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}{Some references}
  \begin{block}{Docs}
    \begin{itemize}
      \item http://en.cppreference.com/w/
    \end{itemize}
  \end{block}

  \begin{block}{Some libraries}
    \begin{itemize}
      \item Boost: generalist library, must-have in C++
        (\url{www.boost.org})
      \item SFML:\@ Simple and Fast Multimedia Library (\url{sfml-dev.org})
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}{Thanks}
  \begin{center}
    {\huge Thanks to mcarton and Kordump}
    \vfill
    {\huge Thank you}
  \end{center}
\end{frame}

\end{document}
