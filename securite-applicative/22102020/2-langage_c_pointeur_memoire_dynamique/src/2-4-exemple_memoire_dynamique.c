#include <stdio.h>
#include <stdlib.h> // Important pour manipuler la mémoire dynamique !
#include <string.h>

int main() {
    char *mon_prenom = (char*) malloc(7); // on oublie pas le caractère nul...
    strcpy(mon_prenom, "Romain");

    printf("%s\n\n", mon_prenom);

    int *nats = (int*) malloc(4*sizeof(int));
    nats[0] = 0;
    nats[1] = 5;
    nats[2] = 10;
    nats[3] = 53;

    for (int i=0; i<4; ++i) {
        printf("%d\n", nats[i]);
    }

    printf("\n");

    nats = realloc(nats, 5*sizeof(int));
    *(nats + 4) = 1000;

    for (int i=0; i<5; ++i) {
        printf("%d\n", nats[i]);
    }

    free(nats);
    free(mon_prenom);

    return 0;
}
