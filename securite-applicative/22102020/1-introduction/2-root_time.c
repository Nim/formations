#include "stdio.h"
#include "string.h"

void get_name(char* name) {
	char buf[512];
	strcpy(buf, name);
	printf("Hello, %s !\n", buf);
}

int main(int argc, char* argv[]) {
	if (argc != 2) {
		printf("usage: %s [name]\n", argv[0]);
		return 1;
	}

	get_name(argv[1]);
	return 0;
}
