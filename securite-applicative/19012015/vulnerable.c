#include <stdio.h>
#include <unistd.h>

void read_pseudo(char *tab) {
    int n = read(0, tab, 4096);
    tab[n] = '\0';
}

int main() {
    char pseudo[20];
    read_pseudo(pseudo);
    printf("Welcome %s", pseudo);
    return 0;
}
